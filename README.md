# Port Folios Maurel Florian

Le but est de présenter le profile de Mr Maurel tout en offrant  une réponse personnalisée suivant les visiteurs.

Il faudra proposer un site responsive pour les tablettes et  téléphones donc mobile first et

le final est de le proposer à des entreprises ou des recruteurs  pour trouver un stage.

Attention aux droits d'auteurs!.



## technologies utilisé

#### Les langages V1:

- HTML
- CSS
- BOOTSTRAP

### langages ajout V2:

 JS en plus.



## User Story

##### En tant que Profesionnel: 

 

-Je souhaite accéder au-dessous du projet et voir la façon ça  façon de travailler.

 

Afin de répondre à cette demande, je vais montrer mon travail en  détail pour un professionnel.Une zone contact avec un GIT en partage sera  disponible.Afin de montrer la technique la mise en forme et les dessous du  projet.



##### En tant que Recruteur :

 

-Je souhaite accéder au parcours professionnel et capter un  maximum d'informations sur le demandeur d'emploi.

 

-Afin de répondre à cette demande je pars du principe que  recruteur ne connaissent rien à la programmation.Utilisation simple pour le  recruteur des flèches pour naviguer de façon fluide .Pas de deuxièmes pages ou  ouverture d'onglets et les langages avec des images simples pour les  compétences.



##### En tant que futur client :

 

-Je souhaite voir un site esthétique qui m'aide à voir des  fonctionnalités pour mes propres projets/

 

-Afin  de répondre à cette demande je vais mettre en place  des technologies demandées  comme Bootstrap/js ... afin d'avoir une interface fluide et animée 



# Mise en forme et graphisme



Les animations évolueront avec la formation .Mais le but est  d'avoir 4 sections qui déroulent grâce à des flèches.

Simple coloré avec un personnage me représentant et un style  pixel art pour une production simple et claire sans trop de  roman.



# maquettes

maquette début:



![maquetteV1](https://gitlab.com/Florian951/foliosmaurel/raw/master/maquettes/maquetteV1.png)





maquette fin projet:



![maquettesV2](https://gitlab.com/Florian951/foliosmaurel/raw/master/maquettes/maquettesV2.png)



maquettes portable:

![maquettesMobile](https://gitlab.com/Florian951/foliosmaurel/raw/master/maquettes/maquettesMobile.png)



## Auteur

Florian Maurel .

## contact

Florian.maurel01@gmail.com

